Splunk Shared Input Context

A small project I have been working on to both learn the splunk JS stack and do something I thought would be useful. It allows multiple inputs to share the results from one search resultset. If choices are made on a control, the other inputs automatically filter their content based on the current set of selections that are made.

It is a *wee* bit hacky at this point. The best way I found to get what I wanted without creating a bunch of sublasses that wrap the functionality I want is to intercept Module init calls from require.js. Then I can attach my own logic onto certain functions. Here is the gist of how I set this up:

1. Rather than having each input be aware of every other input out there, I thought it was a little cleaner to have each input notify the shared source of data (the search manager). The search manager listens for 'change' events on the inputs that have registered with it. When one of these events fires, it adds the selected field/values object to the filters list. The search manager then triggers a 'change:data' event. 

2. This 'change:data' events causes each input to run its 'convertDataToChoices' function. My version of this function checks the shared search manager for a filter list, then does some underscore.js magic to filter out any data objects that don't match the current selection filters.

Both of the above were implemented using the require.js intercept method above on the 'splunkjs/mvc/searchmanager' and 'splunkjs/mvc/basechoiceview' modules. BaseChoiceView was used so that MultiSelectInput, CheckboxGroup, etc get this behavior for free without having to subclass each one.



Still a WIP.