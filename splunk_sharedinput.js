var rjs_intercept = function(r, _) {
    var oldinit = require.s.contexts._.Module.prototype.init;
    var events = require.s.contexts._.config.events;
    
    require.s.contexts._.Module.prototype.init = function () {
        var self = this;
        if (events && events[this.map.id]) {
            _.each(events[this.map.id], function(handlers, type) {
                _.each(handlers, function(handler) {
                    self.on(type, handler.bind(self));
                });
            });
            delete events[this.map.id];
        }
        return oldinit.apply(this, arguments);
    }

}

require(['require', 'underscore'], rjs_intercept);
require.s.contexts._.config.events = {}

var onRequire = function(id, event, callback) {
    var events = require.s.contexts._.config.events;

    events[id] = events[id] || {};
    events[id][event] = events[id][event] || [];
    events[id][event].push(callback);

}
onRequire('splunkjs/mvc/searchmanager', 'defined', function(SearchManager) {
    var _ = require('underscore');
    SearchManager.prototype.filters = [];
    SearchManager.prototype.addNotifier= function(ele) {
            ele.options.filteridx = this.filters.length;
            this.filters.push({field: ele.options.valueField, values: []});
            this.listenTo(ele, "change", function(values, context) {
                this.filters[ele.options.filteridx].values = values;
                this.trigger("change:data");
            });
    }
});

onRequire('splunkjs/mvc/basechoiceview', 'defined', function(BaseChoiceView) {
    var _ = require('underscore');
    var $ = require('jquery');
    var mvc = require('splunkjs/mvc');
    BaseChoiceView.prototype._isNotified = false;

    BaseChoiceView.prototype._onReady = function(cb) {
        var dfd;
        var mymgr = mvc.Components.get(this.settings.get('managerid'));
        if (mymgr) {
            if (!this._isNotified) {
                mymgr.addNotifier(this);
                this._isNotified = true;
            }
            this._readyDfd = $.Deferred();
            dfd = this._readyDfd.promise();
        } else {
            dfd = $.Deferred().resolve();
        }
        if (cb) {
            dfd.always(cb);
        }
        return dfd;        
    }

    BaseChoiceView.prototype.convertDataToChoices = function(data) {
        data = data || this._data || [];
        data = _.isArray(data) ? data : [data]
        
        var valueField = this.settings.get("valueField") || 'value';
        var labelField = this.settings.get("labelField") || valueField;
        var filteredData = data;
        if(this.manager) {
            var filters = _.filter(this.manager.filters, function(filter) {
                return filter.values.length > 0 && filter.field != valueField;
            });
            var filteredData = _.filter(data, function(datum) {
                return _.reduce(filters, function(memo, filter) {
                    return memo && _.reduce(filter.values, function(memo1, value) {
                        return memo1 || (datum[filter.field] === value);
                    }, false);
                }, true);
            });
        }

        var choices = Array.prototype.slice.call(this.settings.get('choices') || []);
        choices = choices.concat(_.map(filteredData, function (row) {
            return {
                label: row[labelField],
                value: row[valueField]
            };
        }));
        choices = _.uniq(choices, false, function (item) {
            return item.label + item.value;
        });
        return choices;
    }


});
